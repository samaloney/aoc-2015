file = open('input.txt', 'r')
lines = file.readlines()
nice_strings = 0
ab = "ab"
cd = "cd"
cc = "pq"
dd = "xy"


for line in lines:
    line = line.strip('\n')
    ab_bool = ab in line
    cd_bool = cd in line
    cc_bool = cc in line
    dd_bool = dd in line
    dbl_bool = False
    if not (ab_bool or cd_bool or cc_bool or dd_bool):
        chardict = {}
        cur_char = ""
        prev_char = ""
        for char in line:
            cur_char = char
            if cur_char == prev_char:
                dbl_bool = True
            
            prev_char = cur_char

            if char in chardict:
                chardict[char] +=1
            else:
                chardict[char] = 1

        vowel_count = 0
        for vowel in 'aeiou':
            if vowel in chardict:
                vowel_count += chardict[vowel]
        
        if vowel_count >= 3 and dbl_bool:
            nice_strings +=1
            print(line)

print(nice_strings)
