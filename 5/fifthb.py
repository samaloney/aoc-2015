import re

file = open('input.txt', 'r')
lines = file.readlines()
#lines =['qjhvhtzxzqqjkmpb', 'xxyxx', 'uurcxstgmygtbstg', 'ieodomkazucvgmuy', 'xyxy']
nice_strings = 0

for line in lines:
    line = line.strip('\n')
    pairs = False
    trio = False
    
    for i in range(len(line)-2):
        if line[i] == line[i+2]:
            trio = True
    
    for i in range(len(line)-1):
        if line.count(line[i:i+2]) > 1:
            pairs = True

    if pairs and trio:
        nice_strings +=1

print(nice_strings)
