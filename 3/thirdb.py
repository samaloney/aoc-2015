file = open('input.txt', 'r')
moves = file.read()

#moves = '^v^v^v^v^v'

santa = {'0x0':1}
robot = {'0x0':1}
sx = rx = 0 
sy = ry = 0

for i, char in enumerate(moves):
    if i % 2 == 0:
        if char == "^":
            sx += 1
        elif char == ">":
            sy += 1
        elif char == "v":
            sx -= 1
        elif char == "<":
            sy -= 1
        
        position_string  = str(sx) + "x" + str(sy)

        if position_string in santa:
            santa[position_string] += 1
        else:
            santa[position_string] = 1

    else:
        if char == "^":
            rx += 1
        elif char == ">":
            ry += 1
        elif char == "v":
            rx -= 1
        elif char == "<":
            ry -= 1
    
        position_string  = str(rx) + "x" + str(ry)

        if position_string in robot:
            robot[position_string] += 1
        else:
            robot[position_string] = 1

combined = dict(list(santa.items()) + list(robot.items()))
print(len(combined))
