file = open('input.txt', 'r')
moves = file.read()

visited = {}
x = 0 
y = 0

for char in moves:
    if char == "^":
        x += 1
    elif char == ">":
        y += 1
    elif char == "v":
        x -= 1
    elif char == "<":
        y -= 1
    
    position_string  = str(x) + "x" + str(y)

    if position_string in visited:
	visited[position_string] += 1
    else:
	visited[position_string] = 1

print(len(visited))
