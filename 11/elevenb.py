import re
old_pass = "hepxxyzz"

def update(password):
    #Reverse so can operate from rigth to left
    password = list(password[::-1])
    for i, char in enumerate(password):
        val = ord(char) + 1
        if val < 123:
            password[i] = chr(val)
            break
        else:
            password[i] = 'a'
            if i >= len(password) - 1:
                password.append('a')
                break
            #else:
            #    password[i+1] = chr(ord(password[i+1]) + 1 )
            

    return "".join(password[::-1])

iol = re.compile(r'[iol]')

def check(password):
    if len(iol.findall(password)) > 0:
        return 0
    else:
        straight = False
        pair = False
        
        for i in range(len(password) - 2):     
            if ord(password[i+1]) == ord(password[i]) + 1 and ord(password[i+2]) == ord(password[i]) + 2:
                straight = True
        
        pairs=[]
        for i in range(len(password) - 1):
            if password[i] == password[i+1]:
                pairs.append(password[i:i+2])

        if len(set(pairs)) > 1:
            pair = True

        if straight and pair:
            return password
        else:
            return 0

found = False
while found != True:
    old_pass = update(old_pass)
    chk  = check(old_pass)
    if chk != 0:
        print(old_pass)
        found = True


