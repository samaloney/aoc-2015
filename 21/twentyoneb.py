import re
import itertools
from operator import add

with open('store.txt', 'r') as store:
    items = store.readlines()
    
    parse = re.compile(r'(\w+)\s+(\d+)\s+(\d+)\s+(\d+)')
    
    curitems = items[1:6]
    weapons = dict()
    for item in curitems:
        name, c, d, a = parse.findall(item)[0]
        weapons[name] = [int(c),int(d),int(a)]

    curitems = items[8:13]
    armor = dict()
    for item in curitems:
        name, c, d, a = parse.findall(item)[0]
        armor[name] = [int(c),int(d),int(a)]

    curitems = items[15:]
    rings = dict()
    for item in curitems:
        name, c, d, a = parse.findall(item.replace(' +', ''))[0]
        rings[name] = [int(c),int(d), int(a)]


with open('input.txt', 'r') as file:
    lines = file.readlines()

    bhp = int(lines[0].split(": ")[1])
    bdam = int(lines[1].split(": ")[1])
    barm = int(lines[2].split(": ")[1])

php = 100
boss = [bhp, bdam, barm]

def play(player, boss):
    ph = player[0]
    pdam = player[1]
    parm = player[2]

    bh = boss[0]
    bdam = boss[1]
    barm = boss[2]

    phit = pdam - barm
    if phit <= 0:
        phit = 1
    bhit = bdam - parm
    if bhit <= 0:
        bhit = 1
    
    bh -= phit

    prounds = bh/phit
    brounds = ph/bhit

    if prounds < brounds:
        return True
    else:
        return False


def lookup(choice):
    total = [0,0,0]
    for group in choice:
        for item in group:
            if item in weapons:
                total = list(map(add, weapons[item], total))
            elif item in armor:
                total = list(map(add, armor[item], total))
            elif item in rings:
                total = list(map(add, rings[item], total))
    
    return total

min_cost = 0
for x in range(2):
    for y in range(3):
        wes = itertools.combinations(weapons.keys(),r=1)
        ars = itertools.combinations(armor.keys(),r=x)
        ris = itertools.combinations(rings.keys(),r=y)
        
        for choice in itertools.product(wes, ars, ris):
            cur_cost = 0
            c, d, a = lookup(choice) 
            if not play([php, d, a], boss):
                if c > min_cost:
                    min_cost = c

print(min_cost)
