file = open('input.txt', 'r')
lines = file.readlines()

ops = {}
for line in lines:
	exp, res = line.strip().split(' -> ')
	
	if exp.isdigit():
		ops[res] = int(exp)
	else:
		ops[res] = exp

def parse(res):
	if res.isdigit():
		return int(res)
	elif type(ops[res]) is int:
		return ops[res]

	tokens = ops[res].split()

	if len(tokens) == 1:
		return parse(tokens[0])

	elif len(tokens) == 2:
		ops[res] = 2**16 - 1 - parse(tokens[1])
		return ops[res]

	elif len(tokens) == 3:
		if tokens[1] == 'AND':
			ops[res] = parse(tokens[0]) & parse(tokens[2])
		elif tokens[1] == 'OR':
			ops[res] = parse(tokens[0]) | parse(tokens[2])
		elif tokens[1] == 'LSHIFT':
			ops[res] = parse(tokens[0]) << parse(tokens[2])
		elif tokens[1] == 'RSHIFT':
			ops[res] = parse(tokens[0]) >> parse(tokens[2])
		
		return ops[res]

ops['b'] = 956 
print(parse('a'))
