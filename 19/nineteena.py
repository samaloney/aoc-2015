import re
from collections import defaultdict


parse_re = re.compile(r'(\w+) => (\w+)')

replacements = defaultdict(list)
molecule = ""
combinations = set()

with open('input.txt', 'r') as file:
    for line in file.readlines():
        match = parse_re.findall(line)
        if len(match) == 1 and len(line) > 1:
            mol, repl = match[0]
            replacements[mol].append(repl)
        else:
            molecule = line.strip()

def replace():
    for k,v in replacements.items():
        matches = re.finditer(k, molecule)
        for match in matches:
            combination = [ molecule[0:match.span()[0]] + mol + molecule[match.span()[1]:] for mol in v]
            for comb in combination:
                combinations.add(comb)

replace()

print(len(combinations))
