import re
from collections import defaultdict
from numpy import vectorize

parse_re = re.compile(r'(\w+) => (\w+)')

replacements = list()
t_molecule = ""
combinations = set()

with open('input.txt', 'r') as file:
    for line in file.readlines():
        match = parse_re.findall(line)
        if len(match) == 1 and len(line) > 1:
            mol, repl = match[0]
            replacements.append((mol, repl))
        else:
            t_molecule = line.strip()

def replace(molecule):
    for k,v in replacements:
        for i in range(len(molecule)):
            if molecule[i:i+len(v)] == v:
                yield molecule[:i] + k + molecule[i+len(v):]
        
replacements = sorted(replacements, key=lambda x: -len(x[1]))
visited = {t_molecule}
m = [t_molecule]

count = 0
while True:
    mm = []
    for i in m:
        for j in replace(i):
            if j in visited:
                continue
            mm.append(j)
            visited.add(j)
            break 
    m = mm
    count +=1

    print(count, len(m), min(vectorize(len)(m)), flush=True)


