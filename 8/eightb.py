import re

file = open('input.txt','r')
lines = file.readlines()

count = 0
for line in lines:
    string = line.strip()
    escaped = '"'.encode() + re.escape(string).encode() + '"'.encode()
    count += len(escaped) - len(string)

print(count)
