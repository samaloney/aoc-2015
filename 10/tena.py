import re

start = '3113322113'

def look_and_say(seq):
    res = ''
    parts = [m.group(0) for m in re.finditer(r"(\d)\1*", seq)]
    for part in parts:
        res += str(len(part)) + part[0]

    return res

for i in range(40): 
    start = look_and_say(start)
    print(str(len(start))) 

