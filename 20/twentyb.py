from collections import defaultdict


def visit_house(num_elves, st=None):
    cur_max = 0
    if st == None:
        st = 0

    houses = defaultdict(int)
    for x in range(st, num_elves):
        count = 0
        for i, inds in enumerate(range(x,num_elves, x+1)):
            if i < 50:
                houses[inds] += ( x + 1 ) * 11
                cur = houses[inds]
                if cur >= cur_max:
                    cur_max = cur
                
                if cur >= 29000000:
                    print(inds+1)
                    return True
    print(cur_max)
    return False

num_elves = 100
while True:
    if not visit_house(num_elves):
        num_elves *= 2
    else:
        break

