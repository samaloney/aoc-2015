import numpy as np

cords = [2947, 3029]
# row is y, col is x
maxdim = max(cords) * 2 

codes = np.zeros([maxdim, maxdim])

def next_code(code):
    return (code * 252533) % 33554393

def next_index(maxs):
    curx = 0
    cury = 0
    while curx <= maxs-1 and cury <= maxs-1:
        yield  [curx, cury]
        if curx == 0:
            curx = cury + 1
            cury = 0
        else:
            curx -= 1
            cury += 1

prev_code = 20151125
inds_gen= next_index(maxdim)
inds0 = next(inds_gen)
codes[inds0[0], inds0[0]] = 20151125

for x, y in inds_gen:
    nc = next_code(prev_code)
    codes[x, y] = next_code(prev_code)
    prev_code = nc

print(codes[2946, 3028])
