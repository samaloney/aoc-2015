import itertools

file = open('input.txt', 'r')
lines = file.readlines()

destinations = []
distances = {}

for line in lines:
    line = line.strip()
    edge, value = line.split(" = ")
    value = int(value)
    locations = edge.split(" to ")
    locations.sort()
    if locations[0] not in destinations:
        destinations.append(locations[0])
    if locations[1] not in destinations:
        destinations.append(locations[1])

    label = "".join(locations)
    if label not in distances:
        distances[label] = value

max_distance=0

permuts = itertools.permutations(destinations)
n_dests = len(destinations)

for permut in permuts:
    cur_distance = 0
    for path in range(n_dests-1):
        pathlabel = list(permut[path:path+2])
        pathlabel.sort()
        pathlabel = "".join(pathlabel)
        cur_distance += distances[pathlabel]

    if cur_distance > max_distance:
        max_distance = cur_distance

print(max_distance)
