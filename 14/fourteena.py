from  math import floor

file = open('input.txt', 'r')
lines = file.readlines()

time = 2503 
reindeer = {}

for line in lines:
    parts = line.split(" ")
    name = parts[0]
    speed = int(parts[3])
    fly = int(parts[6])
    rest = int(parts[13]) 

    distance = speed * fly * ( floor(time /(fly + rest))) + \
        speed * min([fly, (time % (fly + rest))]) 
    reindeer[name] = distance

print(sorted(reindeer.items(), key=lambda x: x[1])[-1])
