from  math import floor

file = open('input.txt', 'r')
lines = file.readlines()

reindeer = {}
for line in lines:
    parts = line.split(" ")
    name = parts[0]
    speed = int(parts[3])
    fly = int(parts[6])
    rest = int(parts[13])
    reindeer[name] = [speed, fly, rest]    

time = 2503 
distances  = {}
points = {name: 0  for name in reindeer}

for t in range(1, time+1):
    for name in reindeer:
        speed, fly, rest = reindeer[name] 
        
        distance = speed * fly * ( floor(t /(fly + rest))) + \
            speed * min([fly, (t % (fly + rest))]) 
        
        distances[name] = distance

    max_dist = max(distances.values())
    
    for name in distances:
        if distances[name] == max_dist:
            points[name] +=1




print(sorted(distances.items(), key=lambda x: x[1])[-1])
print(sorted(points.items(), key=lambda x: x[1])[-1])
