import numpy as nd

file = open('input.txt', 'r')
instructions = file.readlines()

lights = nd.zeros([1000, 1000], dtype=nd.int)


for instruction in instructions:
    instruction = instruction.strip("\n")
    parts = instruction.split()
    if parts[0] == "turn" :
        command = ' '.join(parts[0:2])
        x1, y1 = parts[2].split(',')
        x2, y2 = parts[4].split(',')
    elif parts[0] == "toggle":
        command = parts[0]
        x1, y1 = parts[1].split(',')
        x2, y2 = parts[3].split(',')
 
    x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)
    x2 += 1
    y2 += 1

    if command == 'turn on':
        lights[x1:x2,y1:y2] += 1
    elif command == 'turn off':
        lights[x1:x2,y1:y2] -= 1
        lights[lights < 0] = 0
    elif command == 'toggle':
        lights[x1:x2,y1:y2] += 2

print(lights.sum())
