import itertools

class Player:
    def __init__(self):
        self.hp = 0
        self.mana = 0
        self.armour = 0

    def __str__(self):
        return "Player HP:%s MANA:%s ARMOUR:%s" % (self.hp, self.mana, self.armour)

class Boss:
    def __init__(self):
        self.hp = 0
        self.dam = 0
    
    def __str__(self):
        return "Boss HP:%s DAM:%s" % (self.hp, self.dam) 


def addturn(lst, chs):
    if len(lst) == 0:
        return [[x] for x in chs]
    else:
        out = []
        for i in lst:
            for c in chs:
                t = i.copy()
                t.append(c)
                out.append(t)
        return out


def spell_effects(player, boss, effects):
    for effect in effects:
        if effect == 's':
            player.armour = 7
        elif effect == 'p':
            boss.hp -= 3
        elif effect == 'r':
            player.mana += 101

        effects[effect] -= 1


def play(player, boss, game):
    cost = 0
    effects = {}
    for i, spell in enumerate(game):        
        player.armour = 0
        spell_effects(player, boss, effects)
        effects = {k:v for k,v in effects.items() if v >= 1}        
        if spell in effects:
            #print('Casting spell that is acitve, breaking')
            return (False, 0)
        if spell == 'm' and player.mana >= 53:
            boss.hp  -= 4
            player.mana -= 53
            cost += 53
        elif spell == 'd' and player.mana >= 73:
            boss.hp -= 2
            player.hp += 2
            player.mana -= 73
            cost += 73
        elif spell == 's' and player.mana >= 113:
            effects['s'] = 6 
            player.mana -= 113
            cost += 113
        elif spell == 'p' and player.mana >= 173:
            effects['p'] = 6
            player.mana -= 173
            cost += 173
        elif spell == 'r' and player.mana >= 229:
            effects['r'] = 5
            player.mana -= 229
            cost += 229
        
        player.armour = 0
        spell_effects(player, boss, effects)
        effects = {k:v for k,v in effects.items() if v >= 1}        
        
        player.hp -= boss.dam - player.armour

        if boss.hp <= 0:
            print('Player Win')
            return (True, cost)
        elif player.hp <= 0:
            return (False, 0)
        
    return (True, 0)
       
spells = ['m', 'd', 's', 'p', 'r']
games = []
possibles = []
costs = []
p = Player()
b = Boss()
game = []

while len(costs) <= 1:
    games = addturn(games, spells)
    bad = []
    for game in games:
        p.hp = 50
        p.mana = 500
        #p.hp = 10
        #p.mana = 250
        
        b.hp = 71
        b.dam = 10
        #b.hp = 14
        #b.dam = 8
        
        res = play(p, b, game)
        
        if res[0] == True and res[1] != 0:
            costs.append(res[1])
            print(game)
        elif res[0] == True and res[1] == 0:
            bad.append(game)
    
    games = bad

min(costs)
