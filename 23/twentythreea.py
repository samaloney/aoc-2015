with open('input.txt') as file:
    lines = file.readlines()

#lines = ['inc a', 'jio a, +2', 'tpl a', 'inc a']

instructions = []
registers = {'a':1, 'b':0}
ptr = 0

for line in lines:
    if line[:2] == 'ji':
        jump, offset = line.strip().split(', ')
        inst, reg = jump.split(' ')
        instructions.append([inst, reg, int(offset)])
    else:
        inst, reg = line.strip().split(' ')
        if reg in ['a', 'b']:
            instructions.append([inst, reg])
        else:
           instructions.append([inst, int(reg)])

while ptr < len(instructions):
    inst = instructions[ptr]
    if inst[0] == 'hlf':
        registers[inst[1]] /= 2
        ptr += 1
    elif inst[0] == 'tpl':
        registers[inst[1]] *= 3
        ptr += 1
    elif inst[0] == 'inc':
        registers[inst[1]] += 1
        ptr += 1
    elif inst[0] == 'jmp':
        ptr += inst[1]
    elif inst[0] == 'jie':
        if registers[inst[1]] % 2 == 0:
            ptr += inst[2]
        else:
            ptr += 1
    elif inst[0] == 'jio':
        if registers[inst[1]] == 1:
            ptr += inst[2]
        else:
            ptr += 1


print(registers)
