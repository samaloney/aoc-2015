file = open('input.txt', 'r')
lines = file.readlines()
total_area = 0
for line in lines:
    l, w, h = line.strip('\n').split('x')
    spec = [int(l), int(w), int(h)]
    spec.sort()
    total_area += 2 * spec[0] * spec[1] + 2 * spec[0] * spec[2] + 2 * spec[1] * spec[2] + spec[0]*spec[1]

print(total_area)
