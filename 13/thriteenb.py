import itertools

file = open('input.txt', 'r')
lines = file.readlines()

happy = dict()
for line in lines:
    line = line.strip()
    parts = line.split(" ")
    
    name1 = parts[0]
    name2 = parts[-1].strip('.')
    d_happy = 0
    
    if parts[2] == "gain":
        d_happy = int(parts[3])
    else:
        d_happy = int(parts[3]) * - 1
    
    if name1 in happy:
        happy[name1][name2] = d_happy
    else:
        happy[name1] = {name2: d_happy}

apathetic = {}
for name in happy.keys():
    apathetic[name] = 0
    happy[name]['me'] = 0 

happy['me'] = apathetic

total = 0

arrangements = itertools.permutations(happy.keys())
for arrangement in arrangements:
    cur_tot = 0 
    for i, person in enumerate(arrangement):
        if i < len(arrangement) -1:
            cur_tot += happy[person][arrangement[i-1]] + happy[person][arrangement[i+1]]
        else:
            cur_tot += happy[person][arrangement[i-1]] + happy[person][arrangement[0]]

    if cur_tot > total:
        total = cur_tot

print(total)  
