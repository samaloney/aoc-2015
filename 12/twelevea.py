import re

file = open('input.txt', 'r')
json_msg = file.read()

num_re = re.compile(r'-?\d+')

total = sum([int(item) for item in num_re.findall(json_msg)])
print(total)


