import json
file = open('input.txt', 'r')
json_msg = file.read()
data = json.loads(json_msg)

def parse(items):
    running = 0
    for item in items:
        item_type = type(item)
        if item_type == int:
            running  += item
        elif item_type == list:
            running += parse(item)
        elif item_type == dict:
            vals = item.values()
            if 'red' not in vals:
                running += parse(vals)
    
    return running

print(parse(data))
