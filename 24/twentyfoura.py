from functools import reduce
from operator import mul

import itertools

with open('input.txt', 'r') as file:
    packages = [int(x) for x in file.readlines()]

def clusters(l, n):
    for labels in itertools.product(range(n), repeat=len(l)):
        partition = [[] for i in range(n)]
        for i, label in enumerate(labels):
                partition[label].append(l[i])
        yield partition


#packages = [1,2,3,4,5,7,8, 9, 10, 11]
weight = sum(packages)/3

g1 = []
g2 = []
g3 = []

def min_group(lst, target):
    for x in range(len(lst)):
        group = itertools.permutations(lst, x)
        for choice in group:
            if sum(choice) == target:
                return choice
    else:
        return False  
 

for x in range(len(packages)):
    group = itertools.permutations(packages, x)
    for choice in group:
        if sum(choice) == weight: 
            remaining = [ x for x in packages if x not in choice] 
            for cluster in clusters(remaining, 2):
                weights = [ sum(group) for group in cluster]
                if weights.count(weights[0]) == len(weights) and weights[0] == weight:
                    g1.append(choice)              
                    break
            

            if len(g1) > 0:
                break
    if len(g1) > 0:
        break

g1s = sorted(g1, key=lambda x: reduce(mul, x, 1))
print(reduce(mul, g1s[0], 1))
