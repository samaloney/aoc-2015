import re
file = open('input.txt', 'r')
lines = [line.strip() for line in file.readlines()]

parse_re = re.compile(r'(\w+ \d+): (\w+): (\d+), (\w+): (\d+), (\w+): (\d+)')

signature = {'children': 3,
        'cats': 7,
        'samoyeds': 2,
        'pomeranians': 3,
        'akitas': 0,
        'vizslas': 0,
        'goldfish': 5,
        'trees': 3,
        'cars': 2,
        'perfumes': 1}

aunt = "" 
match = 0

for line in lines:
    cur_match = 0
    name, p1, v1, p2, v2, p3, v3 = parse_re.findall(line)[0] 
    signal = {p1:int(v1), p2:int(v2), p3:int(v3)}
    for key, value in signal.items():
        if key in signature:
            if key == "cats" or key == "trees":
                if signature[key] < signal[key]: 
                    cur_match += 1
            elif key == "pomeranians" or key == "goldfish":
                if signature[key] > signal[key]:
                    cur_match += 1
            elif signal[key] == signature[key]:
                cur_match += 1 
    
    if cur_match > match:
        match = cur_match
        aunt = name

print(aunt)
