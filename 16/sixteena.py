import re
file = open('input.txt', 'r')
lines = [line.strip() for line in file.readlines()]

parse_re = re.compile(r'(\w+ \d+): (\w+): (\d+), (\w+): (\d+), (\w+): (\d+)')

signature = {'children': 3,
        'cats': 7,
        'samoyeds': 2,
        'pomeranians': 3,
        'akitas': 0,
        'vizslas': 0,
        'goldfish': 5,
        'trees': 3,
        'cars': 2,
        'perfumes': 1}

aunt = "" 
match = 0

for line in lines:
    name, p1, v1, p2, v2, p3, v3 = parse_re.findall(line)[0] 
    signal = {p1:int(v1), p2:int(v2), p3:int(v3)}
    cur_match = len({k:v for k,v in signature.items() if ((k in signal) and (signal[k] == signature[k]))})
    if cur_match > match:
        match = cur_match
        aunt = name

print(aunt)
