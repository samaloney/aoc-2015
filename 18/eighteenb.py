import numpy as nd


with open('input.txt', 'r') as file:
    lines = file.readlines()

lights = nd.zeros([100, 100], dtype=nd.int)
lights_next = nd.zeros([100,100],dtype=nd.int)

for i, line in enumerate(lines):
    nums = [1 if char == '#' else 0 for char in line.strip()]
    lights[i,:] = nums

lights[0,0] = 1
lights[0,99] = 1
lights[99,0] = 1
lights[99,99] = 1

def neighbors(row, col):
    if (row - 1) < 0:
        row1 = 0
    else:
        row1 = row - 1

    row2 = row + 2

    if (col - 1) < 0:
        col1 = 0
    else:
        col1 =  col - 1
    
    col2 = col+2

    return lights[row1:row2,col1:col2]


for i in range(100):
    print(i)
    for x, y in nd.ndindex(lights.shape):
        cur_light = lights[x, y]
        area = neighbors(x, y)

        if cur_light == 1:
            if (area.sum() - 1 == 2) or (area.sum() - 1 == 3):
                lights_next[x,y] = 1
            else:
                lights_next[x,y] = 0
        else:
            if area.sum() == 3:
                lights_next[x,y] = 1
            else:
                lights_next[x,y] = 0

    lights = nd.copy(lights_next)
    
    lights[0,0] = 1
    lights[0,99] = 1
    lights[99,0] = 1
    lights[99,99] = 1


print(lights.sum())
