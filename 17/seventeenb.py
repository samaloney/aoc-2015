import itertools

file = open('input.txt', 'r')
containers = [int(line) for line in file.readlines()]
good = []
for i in range(1, len(containers)+1):
    total = 0
    for combination in itertools.combinations(containers, r=i):
        if sum(combination) == 150: 
            total +=1
            good.append(combination)
    if total > 0:
        break

print(total)
