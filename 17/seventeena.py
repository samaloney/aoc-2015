import itertools

file = open('input.txt', 'r')
containers = [int(line) for line in file.readlines()]

total = 0

for i in range(1, len(containers)+1):
    for combination in itertools.combinations(containers, r=i):
        if sum(combination) == 150: total +=1

print(total)
