file = open('input.txt', 'r')
msg = file.read()
opened = 0
closed = 0
for char in msg:
    if char == "(":
        opened += 1
    elif char == ")":
        closed += 1

print (opened - closed)
