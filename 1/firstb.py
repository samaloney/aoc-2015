file = open('input.txt', 'r')
msg = file.read()
current = 0
for i, char in enumerate(msg):
    if char == "(":
        current += 1
    elif char == ")":
        current -= 1
    if current < 0:
        print(i+1)
        break

