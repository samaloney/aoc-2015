import re
import itertools

file = open('input.txt', 'r')
lines = file.readlines()

ingredients = {}
names = []
parse_re = re.compile('(\w+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (\d+)')

for line in lines:
    name, cap, dur, fla, tex, cal = parse_re.findall(line)[0]
    ingredients[name] = [int(cap), int(dur), int(fla), int(tex), int(cal)]
    names.append(name)

score = 0
max_cals = 500

for combination in itertools.product(range(101), repeat=4):
    if sum(combination) == 100:
        ia = [x * combination[0] for x in ingredients[names[0]]]    
        ib = [x * combination[1] for x in ingredients[names[1]]]    
        ic = [x * combination[2] for x in ingredients[names[2]]]   
        id = [x * combination[3] for x in ingredients[names[3]]]   
        
        if (ia[4]+ib[4]+ic[4]+id[4]) == max_cals:
            
            cap = ia[0] + ib[0] + ic[0] + id[0]
            if cap < 0: cap = 0
            
            dur = ia[1] + ib[1] + ic[1] + id[1]
            if dur < 0: dur = 0
            
            fla = ia[2] + ib[2] + ic[2] + id[2]
            if fla < 0: fla = 0
            
            tex = ia[3] + ib[3] + ic[3] + id[3]
            if tex < 0: tex = 0
            
            cur_score = cap*dur*fla*tex 
            #print(cap, dur, fla, tex, cur_score)
            
            if cur_score > score: score = cur_score

print(score)
